import React, { useState, useEffect } from 'react';

import './App.css';

function App() {
  const [practitioners, setPractitioners] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [monthsAndYearsArray, setMonthsAndYearsArray] = useState([]);
  const [practitionerId, setPractitionerId] = useState('');
  const [practionerName, setPractionerName] = useState('');
  const [
    practitionerAllAppointments,
    setPractitionerAllAppointments
  ] = useState([]);

  // Set datepicker initial values
  // Start date on intial load equals 3 years before todays date
  const todaysDate = new Date();
  const [startDate, setStartDate] = useState(
    new Date(todaysDate.setFullYear(todaysDate.getFullYear() - 3))
      .toISOString()
      .slice(0, 10)
  );
  const [endDate, setEndDate] = useState(new Date().toISOString().slice(0, 10));

  const getPractitioners = () => {
    return fetch('practitioners.json', {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        return myJson;
      });
  };

  function getAppointments() {
    return fetch('appointments.json', {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (myJson) {
        // Get all appointments between to startDate and endDate.
        // Ideally this would be done by the server through an API call.

        let appointments = myJson.filter(
          (appointment) =>
            new Date(appointment.date) >= new Date(startDate) &&
            new Date(appointment.date) <= new Date(endDate)
        );

        // Add iso_date to speed up the process of matching appointments to
        // monthsAndYearsArray.
        appointments = appointments.map((appointment) => {
          appointment.iso_date = new Date(appointment.date).toLocaleString(
            'default',
            {
              month: 'short',
              year: 'numeric'
            }
          );
          return appointment;
        });

        return appointments;
      });
  }

  // Months and years between startDate and endDate.
  const getMonthsAndYearsArray = function (startDate, endDate) {
    for (
      var arr = [], date = new Date(startDate);
      date <= endDate;
      date.setDate(date.getDate() + 1)
    ) {
      const monthYear = new Date(date).toLocaleString('default', {
        month: 'short',
        year: 'numeric'
      });
      if (!arr.includes(monthYear)) {
        arr.push(monthYear);
      }
    }
    return arr;
  };

  // Get data for main table to set any array containing practitioner data
  // and tally up revenue and cost totals for the month.
  useEffect(() => {
    Promise.all([getPractitioners(), getAppointments()]).then((values) => {
      let practitioners = values[0];
      // Sort all appointments by practitioners
      const appointments = values[1].sort((a, b) =>
        a.practitioner_id > b.practitioner_id
          ? 1
          : b.practitioner_id > a.practitioner_id
          ? -1
          : 0
      );

      const monthsAndYearsArray = getMonthsAndYearsArray(
        new Date(startDate),
        new Date(endDate)
      );

      practitioners = practitioners.map((practitioner) => {
        practitioner.appointments = appointments.filter(
          (appointment) => appointment.practitioner_id === practitioner.id
        );

        practitioner.appointmentRevenueAndCostByMonth = monthsAndYearsArray.map(
          (month) => {
            const appointments = practitioner.appointments.filter(
              (a) => a.iso_date === month
            );

            return {
              id: Math.random(),
              date: month,
              revenue: appointments
                .map((appointment) => Number(appointment.revenue))
                .reduce((a, b) => a + b, 0)
                .toFixed(2),
              cost: appointments
                .map((appointment) => Number(appointment.cost))
                .reduce((a, b) => a + b, 0)
                .toFixed(2)
            };
          }
        );

        return practitioner;
      });

      setPractitioners(practitioners);
      setMonthsAndYearsArray(monthsAndYearsArray);
    });
  }, [startDate, endDate]);

  // Get all the appointments for a selected practioner.
  useEffect(() => {
    Promise.all([getAppointments()]).then((values) => {
      // Sort all appointments by practitioners
      const appointments = values[0];

      // Get all appointments for selected practioner
      // Sort apointments by date for simpler viewing
      // Fix date to present in Australian format
      const practitionerAllAppointments = appointments
        .filter((appointment) => appointment.practitioner_id === practitionerId)
        .sort((a, b) => new Date(a.date) - new Date(b.date))
        .map((appointment) => {
          appointment.date = new Date(appointment.date).toLocaleString(
            'en-AU',
            {
              day: 'numeric',
              month: 'numeric',
              year: 'numeric'
            }
          );
          appointment.cost = appointment.cost.toFixed(2);
          appointment.revenue = appointment.revenue.toFixed(2);
          return appointment;
        });

      setPractitionerAllAppointments(practitionerAllAppointments);
    });
  }, [practitionerId]);

  return (
    <div className="App">
      <main>
        <div className="date-field margin-bottom-md">
          <label htmlFor="startDate">
            Start date
            <input
              type="date"
              className="margin-right-sm"
              id="startDate"
              name="startDate"
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
            />
          </label>
          <label htmlFor="endDate">
            End date
            <input
              type="date"
              id="endDate"
              name="endDate"
              min={new Date(startDate)}
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
            />
          </label>
        </div>

        <h3>Practitioner revenue table</h3>
        <div className="table-responsive">
          <table>
            <thead>
              <tr>
                <th className="width-of-name-cell"></th>
                <th className="width-of-type-cell"></th>
                {monthsAndYearsArray.map((monthAndYear) => (
                  <th key={monthAndYear} className="width-of-data-cell">
                    {monthAndYear}
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {practitioners.map((practitioner) => (
                <tr key={practitioner.id}>
                  <td className="width-of-name-cell">
                    <button
                      className="btn btn-link"
                      onClick={() => {
                        setPractitionerId(practitioner.id);
                        setPractionerName(practitioner.name);
                      }}
                    >
                      {practitioner.name}
                    </button>
                  </td>
                  <td className="width-of-type-cell">
                    <table className="no-border">
                      <tbody className="no-border">
                        <tr className="no-borde">
                          <td className="width-of-type-cell no-border">
                            Revenue
                          </td>
                        </tr>
                        <tr>
                          <td className="width-of-type-cell no-border cost-border">
                            Cost
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                  {practitioner.appointmentRevenueAndCostByMonth.map(
                    (appointment) => (
                      <td key={appointment.id} className="width-of-data-cell">
                        <table className="no-border">
                          <tbody className="no-border">
                            <tr className="no-borde">
                              <td className="width-of-data-cell no-border">
                                $
                                {appointment.revenue
                                  .toString()
                                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                              </td>
                            </tr>
                            <tr>
                              <td className="width-of-data-cell no-border cost-border">
                                $
                                {appointment.cost
                                  .toString()
                                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    )
                  )}
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <div>
          <br />
          <br />
          <br />

          {practitionerId ? (
            <div>
              <h3>Appointments for {practionerName}</h3>

              <div className="table-responsive">
                <table>
                  <thead>
                    <tr>
                      <th className="">Date</th>
                      <th className="">Client</th>
                      <th className="">Revenue</th>
                      <th className="">Cost</th>
                      <th className="">Duration</th>
                      <th className="">Appointment type</th>
                    </tr>
                  </thead>
                  <tbody>
                    {practitionerAllAppointments.map((appointment) => (
                      <tr key={appointment.id}>
                        <td className="">{appointment.date}</td>
                        <td className="">{appointment.client_name}</td>
                        <td className="">
                          $
                          {appointment.revenue
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        </td>
                        <td className="">
                          $
                          {appointment.cost
                            .toString()
                            .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        </td>
                        <td className="">{appointment.duration}</td>
                        <td className="">{appointment.appointment_type}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          ) : null}
        </div>
      </main>
    </div>
  );
}

export default App;
